<?php require_once "./code.php" ?>
<!-- s02-a1 activity -->
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>s02 Activity</title>
</head>
<body>
	<h2>Divisibles of Five</h2>

	<?php forLoop(); ?>

	<h1>Array Manipulation</h1>

	<?php array_push($students, 'John Smith') ?>

	<pre><?php var_dump($students) ?></pre>

	<pre><?php echo count($students) ?></pre>

	<?php array_push($students, 'Jane Smith') ?>

	<pre><?php var_dump($students) ?></pre>

	<pre><?php echo count($students) ?></pre>

	<?php array_shift($students) ?>

	<pre><?php var_dump($students) ?></pre>

	<pre><?php echo count($students) ?></pre>

</body>
</html>